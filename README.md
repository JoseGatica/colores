<h1>Colores</h1>

<p>Es una app web simple a través de la cual usted podrá tener acceso fácil a los colores corporativos de la empresa; lo anterior con el fin de tener un estándar con respecto a los colores utilizados en nuestro sitio web, publicidad y otros.</p>

<h1>¿Qué utilizamos en este proyecto?</h1>

<p>Para construir esta aplicación utilicé lo siguiente:
    <ul>
        <li>Angular, como framework front end</li>
        <li>Los grid fueron hechos utilizando Flexbox</li>
        <li>El control de versiones fue administrado con Git</li>
    </ul>
</p>

<h1>Instalación y ejecución</h1>

<p>Primero es necesario descomprimir colores.zip, y luego ingresar a la carpeta descomprimida.<br> Luego, debe instalar Angular:<br>
<ul>
    <li>npm install -g @angular/cli@latest</li>
</ul>
<br>
Una vez instalado Angular, abrir la terminal dentro del directorio /colores y ejecutar el siguiente comando:<br>
<ul>
    <li>ng serve -o (así, para que se abra en el navegador en localhost usando el puerto 4200)</li>
</ul>
</p>

