import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', './flexboxgrid.min.css']
})
export class AppComponent {
  title = 'colores';
}
